## Mastermind

# Rappel des règles
- Le jeu a une suite de 4 pions de couleur en mémoire
- Le joueur doit deviner cette suite de couleur, au bout de 16 essais il a perdu
- A chaque tentative, le jeu lui réponds une indication : pour chaque pion, s'il est bien placé, s'il est présent dans la solution finale mais mal placée, ou vide si la couleur n'existe pas

## Game of life

# Rappel des règles
Le jeu de la vie (Game of life) par le génialissime Ron Conway
A chaque tour :
- Chaque cellule avec moins de deux voisins meurt (sous population)
- Chaque cellule qui a plus de 3 voisins meurt (surpopulation)
- Chaque cellule avec 2 ou 3 voisins vit à la prochaine génération
- Chaque cellulle vide avec exactement 3 voisins devient vivante


La valeur de retour de la fonction doit être un array de dimension 2. L'univers est infini dans les directions x et y. Si tout est mort, on renvoit `[[]]`.
Les cellules voisines d'une cellule correspondent aux 8 autour.
Toutes les celulles sont vides mise à part celles de départ.
Si besoin de représentation, 0 peut représenter une cellule morte, et 1 une cellule vivante.